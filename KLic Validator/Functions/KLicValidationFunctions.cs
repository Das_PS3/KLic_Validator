﻿using Extra.Utilities;
using PKG_Extractor;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace KLic_Validator.Functions
{
    internal class KLicValidationFunctions
    {
        private static readonly byte[] NPDRM_OMAC_Key2 = { 0x6B, 0xA5, 0x29, 0x76, 0xEF, 0xDA, 0x16, 0xEF, 0x3C, 0x33, 0x9F, 0xB2, 0x97, 0x1E, 0x25, 0x6B };

        private enum FileType
        {
            EDAT,
            SELF,
            SPRX,
        }

        internal static PKGHeader GetPKGHeader(string pkgFile)
        {
            return File.Exists(pkgFile) ? GetFilePKGHeader(pkgFile) : GetLinkPKGHeader(pkgFile);
        }

        internal static void ValidateKLics(string pkgFile, string[] klicsRaw, PKGHeader header, string klicDirectory)
        {
            foreach (var entry in header.Entries)
                if (entry.Filename.EndsWith("iso.bin.edat", StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] fileHeader = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, 0x100, entry, header) : GetLinkPKGFile(pkgFile, 0x100, entry, header);

                    ValidateKlics(fileHeader, klicsRaw, FileType.EDAT, header, klicDirectory, pkgFile);

                    return;
                }

            foreach (var entry in header.Entries)
                if (entry.Filename.EndsWith(".edat", StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] fileHeader = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, 0x100, entry, header) : GetLinkPKGFile(pkgFile, 0x100, entry, header);

                    ValidateKlics(fileHeader, klicsRaw, FileType.EDAT, header, klicDirectory, pkgFile);

                    return;
                }

            foreach (var entry in header.Entries)
            {
                if (entry.Filename.EndsWith(".self", StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] fileHeader = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, 0x500, entry, header) : GetLinkPKGFile(pkgFile, 0x500, entry, header);

                    byte[] isNPD = new byte[1];

                    Buffer.BlockCopy(fileHeader, 0x7F, isNPD, 0, isNPD.Length);

                    if (isNPD[0] != 0x08)
                        continue;

                    ValidateKlics(fileHeader, klicsRaw, FileType.SELF, header, klicDirectory, pkgFile);

                    return;
                }

                if (entry.Filename.EndsWith(".sprx", StringComparison.InvariantCultureIgnoreCase))
                {
                    byte[] fileHeader = File.Exists(pkgFile) ? GetFilePKGFile(pkgFile, 0x500, entry, header) : GetLinkPKGFile(pkgFile, 0x500, entry, header);

                    byte[] isNPD = new byte[1];

                    Buffer.BlockCopy(fileHeader, 0x7F, isNPD, 0, isNPD.Length);

                    if (isNPD[0] != 0x08)
                        continue;

                    ValidateKlics(fileHeader, klicsRaw, FileType.SPRX, header, klicDirectory, pkgFile);

                    return;
                }
            }

            string @out, outputFile, outputPath;

            if (File.Exists(pkgFile))
                @out = string.Empty;
            else
                @out = "_" + Path.GetFileName(new Uri(pkgFile).AbsolutePath).Substring(0, 4);

            foreach (var entry in header.Entries)
            {
                if (entry.Filename.EndsWith("EBOOT.BIN", StringComparison.InvariantCultureIgnoreCase))
                {
                    outputFile = header.ContentID + @out + ".klic_not_needed";

                    outputPath = Path.Combine(klicDirectory, outputFile);

                    using (File.Create(outputPath)) { }

                    return;
                }
            }

            outputFile = header.ContentID + @out + ".no_npdrm_files";

            outputPath = Path.Combine(klicDirectory, outputFile);

            using (File.Create(outputPath)) { }

            return;
        }

        private static long GetCheckHeaderEnd(byte[] headerCheckRaw)
        {
            using (var ms = new MemoryStream(headerCheckRaw))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x14;
                byte[] fileCountRaw = br.ReadBytes(4);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(fileCountRaw);

                long fileCount = BitConverter.ToInt32(fileCountRaw, 0);

                ms.Position = 0x20;
                byte[] encryptedDataOffsetRaw = br.ReadBytes(8);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(encryptedDataOffsetRaw);

                long encryptedDataOffset = BitConverter.ToInt64(encryptedDataOffsetRaw, 0);

                ms.Position = encryptedDataOffset;

                return encryptedDataOffset + 0x10;
            }
        }

        private static byte[] GetFilePKGFile(string pkgFile, long requestedSize, PKGEntry entry, PKGHeader header)
        {
            long startOffset = header.EncryptedDataOffset + entry.FileContentsOffset;

            if (requestedSize <= 0)
                requestedSize = entry.FileContentsSize;
            else if (requestedSize > entry.FileContentsSize)
                requestedSize = entry.FileContentsSize;

            byte[] rawFile;

            using (var fs = File.Open(pkgFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var br = new BinaryReader(fs))
            {
                fs.Position = startOffset;

                rawFile = br.ReadBytes((int)requestedSize);
            }

            var extractor = new PKGExtractor();

            return extractor.DecryptRetailFile(header, entry, rawFile);
        }

        private static PKGHeader GetFilePKGHeader(string pkgFile)
        {
            using (var fs = File.Open(pkgFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var br = new BinaryReader(fs))
            {
                byte[] headerCheckRaw = br.ReadBytes(0x28);

                if (headerCheckRaw == null)
                    return null;

                long checkHeaderEnd = GetCheckHeaderEnd(headerCheckRaw);

                fs.Position = 0;

                byte[] rawCheckHeader = br.ReadBytes((int)checkHeaderEnd);

                var checkHeader = new PKGHeader(rawCheckHeader);

                if (!checkHeader.IsFinalizedValid || !checkHeader.IsMagicValid || !checkHeader.IsTypeValid)
                    return null;

                long lastOffset = checkHeader.Entries.FirstOrDefault().FileContentsOffset;

                fs.Position = 0;

                byte[] rawHeader = br.ReadBytes((int)(checkHeader.EncryptedDataOffset + lastOffset));

                checkHeader = null;

                var header = new PKGHeader(rawHeader);

                return header;
            }
        }

        private static byte[] GetLinkPKGFile(string link, long requestedSize, PKGEntry entry, PKGHeader header)
        {
            long startOffset = header.EncryptedDataOffset + entry.FileContentsOffset;

            if (requestedSize <= 0)
                requestedSize = entry.FileContentsSize;
            else if (requestedSize > entry.FileContentsSize)
                requestedSize = entry.FileContentsSize;

            if (link.EndsWith("_00.pkg"))
            {
                long pkgPart = 4687500000;

                for (int i = 0; i < 25; i++)
                    if (startOffset < pkgPart * (i + 1))
                    {
                        link = Regex.Replace(link, @"_\d\d\.pkg", string.Format("_{0:00}.pkg", i));

                        startOffset = startOffset - (pkgPart * i);

                        break;
                    }
            }

            byte[] rawFile = null;

            do
                Retry.Do(() => { rawFile = GetRawData(link, startOffset, startOffset + requestedSize); }, 1000, 5);
            while (rawFile == null || rawFile.Length == 0);

            var extractor = new PKGExtractor();

            return extractor.DecryptRetailFile(header, entry, rawFile);
        }

        private static PKGHeader GetLinkPKGHeader(string link)
        {
            byte[] headerCheckRaw = GetRawData(link, 0, 0x28);

            if (headerCheckRaw == null)
                return null;

            long checkHeaderEnd = GetCheckHeaderEnd(headerCheckRaw);

            byte[] rawCheckHeader = GetRawData(link, 0, checkHeaderEnd);

            var checkHeader = new PKGHeader(rawCheckHeader);

            if (!checkHeader.IsFinalizedValid || !checkHeader.IsMagicValid || !checkHeader.IsTypeValid)
                return null;

            long lastOffset = checkHeader.Entries.FirstOrDefault().FileContentsOffset;

            byte[] rawHeader = GetRawData(link, 0, checkHeader.EncryptedDataOffset + lastOffset);

            checkHeader = null;

            var header = new PKGHeader(rawHeader);

            return header;
        }

        private static byte[] GetRawData(string link, long start, long end)
        {
            var getPKGHeader = (HttpWebRequest)WebRequest.Create(link);
            getPKGHeader.UserAgent = string.Empty;
            getPKGHeader.AddRange(start, end);

            try
            {
                using (var response = getPKGHeader.GetResponse())
                using (var data = response.GetResponseStream())
                using (var reader = new BinaryReader(data))
                {
                    byte[] rawPKGHeader = reader.ReadBytes((int)(end - start));

                    return rawPKGHeader;
                }
            }
            catch (WebException)
            {
                return null;
            }
        }

        private static bool IsKLicValid(byte[] fileHeader, FileType fileType, byte[] klic)
        {
            using (var ms = new MemoryStream(fileHeader))
            using (var br = new BinaryReader(ms))
            {
                if (fileType == FileType.EDAT)
                    ms.Position = 0x60;

                if (fileType == FileType.SELF || fileType == FileType.SPRX)
                {
                    ms.Position = 0x0E;

                    byte[] klicHashOffset = br.ReadBytes(2);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(klicHashOffset);

                    short position = BitConverter.ToInt16(klicHashOffset, 0);

                    ms.Position = position;
                }

                byte[] klicHash = br.ReadBytes(0x10);

                ms.Position -= 0x70;

                byte[] npdHash2Seed = br.ReadBytes(0x60);

                byte[] xoredKlic = CryptographicEngines.XOR(klic, NPDRM_OMAC_Key2);

                byte[] npdHash2 = CryptographicEngines.AESCMAC(npdHash2Seed, xoredKlic);

                return CryptographicEngines.CompareBytes(npdHash2, 0, klicHash, 0, klicHash.Length);
            }
        }

        private static bool ValidateKlics(byte[] fileHeader, string[] klicsRaw, FileType fileType, PKGHeader header, string klicDirectory, string pkgFile)
        {
            foreach (var klicRaw in klicsRaw)
            {
                byte[] klic = MiscUtils.HexStringToByteArray(klicRaw.Substring(0, 0x20));

                if (IsKLicValid(fileHeader, fileType, klic))
                {
                    string output = Path.Combine(klicDirectory, header.ContentID + ".klic");

                    File.WriteAllBytes(output, klic);

                    return true;
                }
            }

            string @out;

            if (File.Exists(pkgFile))
                @out = string.Empty;
            else
                @out = "_" + Path.GetFileName(new Uri(pkgFile).AbsolutePath).Substring(0, 4);

            string outputFile = header.ContentID + @out + ".klic_not_found";

            string outputPath = Path.Combine(klicDirectory, outputFile);

            using (File.Create(outputPath)) { }

            File.AppendAllText("klic_not_found.txt", pkgFile + Environment.NewLine);

            return false;
        }
    }
}
