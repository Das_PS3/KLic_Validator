﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Extra.Utilities
{
    internal static class Retry
    {
        internal static void Do(Action action, int retryInterval, int retryCount = 3)
        {
            Do<object>(() => { action(); return null; }, retryInterval, retryCount);
        }

        internal static T Do<T>(Func<T> action, int retryInterval, int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    if (retry > 0)
                        Thread.Sleep(retryInterval);

                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
