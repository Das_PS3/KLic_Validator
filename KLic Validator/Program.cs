﻿using KLic_Validator.Functions;
using PKG_Extractor;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace KLic_Validator
{
    internal class Program
    {
        private const string KLicDirectory = "klic";
        private const string LinkRegex = @"http://.*?\.pkg";
        private const string SplitNonFirstRegex = @"_\d[1-9]\.pkg";

        private static bool CheckLink(string link)
        {
            if (link.Contains("/PCS") || link.Contains("/prod/CUSA"))
            {
                Console.WriteLine("ERROR: Unsupported pkg link: " + link);

                return false;
            }

            if (File.Exists(link))
                using (var fs = File.OpenRead(link))
                using (var br = new BinaryReader(fs))
                {
                    byte[] magic = { 0x7F, 0x50, 0x4B, 0x47 };

                    byte[] thisMagic = br.ReadBytes(4);

                    if (!Enumerable.SequenceEqual(magic, thisMagic))
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file: " + link);

                        return false;
                    }

                    if (br.ReadByte() != 0x80)
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file: " + link);

                        return false;
                    }

                    fs.Position = 0x30;

                    byte[] cidRaw = br.ReadBytes(0x24);

                    string cid = Encoding.UTF8.GetString(cidRaw);

                    if (cid.Contains("-PCS"))
                    {
                        Console.WriteLine("ERROR: Unsupported pkg file: " + link);

                        return false;
                    }
                }
            else
            {
                var request = (HttpWebRequest)WebRequest.Create(link);
                request.UserAgent = string.Empty;
                request.AddRange(0x4, 0x5);

                using (var response = request.GetResponse())
                using (var data = response.GetResponseStream())
                    if (data.ReadByte() != 0x80)
                    {
                        Console.WriteLine("ERROR: Unsupported pkg link: " + link);

                        return false;
                    }
            }

            return true;
        }

        private static bool CheckParameters(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine(Assembly.GetExecutingAssembly().GetName().Name + " v" + Assembly.GetExecutingAssembly().GetName().Version + " by Dasanko, written for catalinnc." + Environment.NewLine);

                Console.WriteLine("Missing parameters." + Environment.NewLine);

                Console.WriteLine("Usage: " + Environment.NewLine);

                Console.WriteLine('"' + Assembly.GetExecutingAssembly().GetName().Name + "\" <link|links_file> <klic|klics_file>" + Environment.NewLine);

                Console.WriteLine("Example: \"" + Assembly.GetExecutingAssembly().GetName().Name +
                                  "\" http://zeus.dl.playstation.net/cdn/EP1018/NPEB01032_00/VxGedVinRnApAqiMFHrUkxuNftQhuAQKCipAOyHLrLnkXfBleTHwFmnQiBnYdrca.pkg klics.txt");

                return false;
            }

            if (args.Length == 2 && (!File.Exists(args[0])) || !File.Exists(args[1]))
            {
                Console.WriteLine("ERROR: Invalid links file or klics file.");

                return false;
            }

            return true;
        }

        private static void Main(string[] args)
        {
            if (!CheckParameters(args))
                return;

            Console.WriteLine(string.Format("[{0}] Now working, please wait...", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));

            if (!Directory.Exists(KLicDirectory))
                Directory.CreateDirectory(KLicDirectory);

            string[] links = File.ReadAllLines(args[0]);
            string[] klics = File.ReadAllLines(args[1]);

            for (int i = 0; i < links.Length; i++)
            {
                Console.WriteLine(string.Format("[{0}] Processing link {1} of {2}", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss"), i + 1, links.Length));

                if (!CheckLink(links[i]))
                    continue;

                if (Regex.IsMatch(links[i], SplitNonFirstRegex))
                    links[i] = Regex.Replace(links[i], SplitNonFirstRegex, @"_00.pkg");

                ProcessParameters(links[i], klics);
            }

            Console.WriteLine(string.Format("[{0}] Work complete!", DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss")));
        }

        private static void ProcessParameters(string link, string[] klicsRaw)
        {
            PKGHeader header = KLicValidationFunctions.GetPKGHeader(link);

            KLicValidationFunctions.ValidateKLics(link, klicsRaw, header, KLicDirectory);
        }
    }
}
